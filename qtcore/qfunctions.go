package qtcore

import "unsafe"
import "github.com/kitech/qt.go/qtrt"

func init_unused_10261() {
	if false {
		_ = unsafe.Pointer(uintptr(0))
	}
	if false {
		_ = Voidptr(uintptr(0))
	}
	if false {
		qtrt.KeepMe()
	}
}

//  header block end
