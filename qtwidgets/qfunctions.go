package qtwidgets

import "unsafe"
import "github.com/kitech/qt.go/qtrt"
import "github.com/kitech/qt.go/qtcore"
import "github.com/kitech/qt.go/qtgui"

func init_unused_10283() {
	if false {
		_ = unsafe.Pointer(uintptr(0))
	}
	if false {
		_ = Voidptr(uintptr(0))
	}
	if false {
		qtrt.KeepMe()
	}
	if false {
		qtcore.KeepMe()
	}
	if false {
		qtgui.KeepMe()
	}
}

//  header block end
