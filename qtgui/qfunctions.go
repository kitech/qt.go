package qtgui

import "unsafe"
import "github.com/kitech/qt.go/qtrt"
import "github.com/kitech/qt.go/qtcore"

func init_unused_10263() {
	if false {
		_ = unsafe.Pointer(uintptr(0))
	}
	if false {
		_ = Voidptr(uintptr(0))
	}
	if false {
		qtrt.KeepMe()
	}
	if false {
		qtcore.KeepMe()
	}
}

//  header block end
